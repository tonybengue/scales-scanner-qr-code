# Scales Scanner 
Le projet se trouve à l'adresse suivante : https://gitlab.com/tonybengue/scales-scanner  
Il permet de pouvoir effectuer un scan des balances sur place à partir d'un QR Code apposé sur celle ci. L'utilisateur, lors du lancement de l'application est alors invité à se connecter via un formulaire sécurisé puis une fois connecté il peut récupérer les données via scan.

## Documentation
### Markdown
https://www.markdownguide.org/basic-syntax/

## Problématiques
### API Php
https://ionicdon.com/how-to-write-read-and-display-data-from-mysql-database-in-ionic-app/

### Connexion depuis mysql
https://linuxize.com/post/mysql-ssh-tunnel/

Pas d'accès distant à la base de données
  Changer de mode d'hébergement
  Tunnel SSH

Pas de possibilité d'installer nodeJS
  changer d'offre
  VPS

### IONIC
ionic serve
ionic serve --source-map=false

## TODO
Communication angular / php
  Envoit des données en json depuis angular à php en mettant l'en tête pour les json

### Validation
  https://ionicthemes.com/tutorials/about/forms-and-validation-in-ionic

### Authentification
  https://stackoverflow.com/questions/44936354/how-to-efficiently-store-and-use-authentication-in-ionic-3

### QR Code
  Ajout scanner
  Récupération de l'IMEI
  Affichage

  ionic cordova plugin add phonegap-plugin-barcodescanner
  npm install --save @ionic-native/barcode-scanner

### Plateforme android de cordova
  ionic cordova platform remove android
  ionic cordova platform add android

Lancer sur mobile l'application
  ionic cordova run android -l --no-native-run --external --verbose
  ionic cordova run android -l --host=192.168.0.16 --no-native-run --verbose

Créer APK 
  ionic cordova build android --release
  ionic cordova build android --prod

  ionic cordova run android --device

Bug erreur cleartext
  changer les reglages
    Boulot\Sbeeh\Ionic QRCode\resources\android\xml\network_security_config.xml

    
-- Bug
https://forum.ionicframework.com/t/could-not-find-plugin-proposal-numeric-separator/185556/12

gradle -v
cordova requirements android
ionic info
ionic cordava info
ionic cordova plugins

https://www.youtube.com/watch?v=oYChA-rPgpI
https://cordova.apache.org/docs/fr/latest/guide/platforms/android/
https://ionicframework.com/docs/developing/android

Ionic avec database 
https://forum.ionicframework.com/t/ionic-4-how-to-connect-the-device-app-with-local-database-server/174263/5

Licences
  https://stackoverflow.com/questions/40383323/cant-accept-license-agreement-android-sdk-platform-24
  sdkmanager --update && sdkmanager --licenses
  %ANDROID_HOME%/tools/bin/sdkmanager --licenses
  mkdir "%ANDROID_HOME%\licenses"
  echo |set /p="8933bad161af4178b1185d1a37fbf41ea5269c55" > "%ANDROID_HOME%\licences\android-sdk-license"

Guard
  https://jasonwatmore.com/fr/post/2019/06/10/angular-8-tutoriel-et-exemple-sur-lenregistrement-et-lauthentification-des-utilisateurs
  https://www.joshmorony.com/prevent-access-to-pages-in-ionic-with-angular-route-guards/
  https://modev.net/angular-6-implement-auth-guard-to-your-project/

Flash Message Submit Form
  http://sachinchoolur.github.io/angular-flash/
  https://www.joshmorony.com/creating-a-flash-message-service-in-ionic/
  
## Problèmes
Error message formulaire à la soumission

## Questionnements
JWT?
  https://jwt.io/introduction/
  https://blog.ippon.fr/2017/10/12/preuve-dauthentification-avec-jwt/
  https://www.techiediaries.com/php-jwt-authentication-tutorial/
  https://www.techiediaries.com/angular/jwt-authentication-angular-9-example/
  https://www.youtube.com/watch?v=2PPSXonhIck
  En tête en base 64

## Liens à lire
https://scotch.io/courses/build-your-first-angular-website/creating-an-angular-header-and-footer
https://www.codeproject.com/Articles/1005150/Posting-Data-from-Ionic-App-to-PHP-Server
https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
https://codeforgeek.com/angular-post-request-php/
https://www.1min30.com/developpement-web/projet-web-6901
https://blog.sodifrance.fr/angular-4-gestion-de-lauthentification-et-des-habilitations/
https://www.9lessons.info/2017/06/ionic-angular-php-login-restful-api.html
https://fahmidasclassroom.com/register-and-login-system-using-angular-8-php-and-mysql/
https://www.youtube.com/watch?v=ee3DngM3NXE

https://blog.flicher.net/ionic-4-user-registration-login-tutorial/
https://jasonwatmore.com/post/2019/06/22/angular-8-jwt-authentication-example-tutorial
https://bezkoder.com/angular-jwt-authentication/
https://www.truecodex.com/course/angular-project-training/login-and-logout-using-web-api-with-token-based-authentication-angular

