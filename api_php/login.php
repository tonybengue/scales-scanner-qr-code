<?php
// https://www.codeproject.com/Articles/1005150/Posting-Data-from-Ionic-App-to-PHP-Server
// https://www.youtube.com/watch?v=VroNPi1t3Cc
// Headers to send the request with json
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');
header('Access-Control-Allow-Methods: *');
header("Access-Control-Max-Age: 3600");
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

// require __DIR__ ."../vendor/autoload.php";
require "../vendor/autoload.php";
require_once 'connection.php'; 

use \Firebase\JWT\JWT;

// http://localhost:8001/_boulot/Sbeeh/Ionic%20QRCode/api_php/
$url = "D:\Logiciels\Cours_Formations\Serveurs\PhP_SQL\Laragon_BEST\www\_Boulot\Sbeeh\Ionic_App\src\app\home\home.page.ts";

switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        echo json_encode("yo");
        // foreach (getallheaders() as $name => $value) { 
        //     echo "$name: $value <br />";
        // }
        break;

    case "POST":
        // Get datas sent by Angular from the body request
        $postdata = file_get_contents("php://input");

        if (isset($postdata) && !empty($postdata)) {
            $request = json_decode($postdata); // decode the datas from angular

            if(isset($request->email) && isset($request->password)) {
                // Form values from Angular and Sanitize
                $email = htmlspecialchars(strip_tags(trim($request->email)));
                $password = htmlspecialchars(strip_tags(trim($request->password)));

                // Get the user from the database
                $stmt = $bdd->prepare("SELECT id, last_name, first_name, email, password FROM users WHERE email=:email LIMIT 0,1");
                $stmt->bindParam(':email', $email);
                $stmt->execute();
                
                // Set the resulting array to associative
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
    
                // Get the user
                $results = $stmt->fetch();
            }

            // Hash the password with bcrypt
            // $hash = password_hash($password, PASSWORD_DEFAULT);
            if($results) {
                // Compare passwords between Angular and the db (bcrypt hash)
                if (password_verify($password, $results["password"])) {
                    $userId = $results['id'];
                    $userFirstName = $results['first_name'];
                    $userLastName = $results['last_name'];
                    $userEmail = $results['email'];
                    
                    $secretKey = "the_secret_key";
                    
                    /* Generate a JWT Token */
                    $token = createToken($userId, $userFirstName, $userLastName, $userEmail);
                    $tokenEncoded = mb_convert_encoding($token, 'UTF-8', 'UTF-8');
                    $jwtToken = JWT::encode($tokenEncoded, $secretKey);
                    
                    // associative array to send the datas as obj for angular
                    $datas = [
                        'message' => 'User connected',
                        'jwt' => $jwtToken,
                    ];

                    http_response_code(200);
                    echo json_encode($datas, true);
                } else {
                    http_response_code(400);
                    echo json_encode(['message' => "Erreur dans le mot de passe"], true);
                }
            } else {
                http_response_code(400);
                echo json_encode(['message' => "Erreur dans l'email"], true);
            }
        }
        break;

    case "PUT":
        echo("put");
        break;

    case "DELETE":
        echo("delete");
        break;
}

function createToken($id, $firstname, $lastname, $email) {
    $issuer = "ruches"; // this can be the servername (identification)
    $audience = "standard"; // audience
    $subject = ""; // subject of the token
    $issuedat = time(); // Age of the token
    $notbefore = $issuedat + 10; // Time before the token is ok
    $expireDate = $issuedat + 60; // Time before expiration
    $token = array(
        "iss" => $issuer, 
        "sub" => $subject,
        "aud" => $audience, 
        "iat" => $issuedat,
        "nbf" => $notbefore,
        "exp" => $expireDate,
        "data" => array(
            "id" => $id,
            "firstname" => $firstname,
            "lastname" => $lastname,
            "email" => $email
    ));

    return $token;
}

$bdd = null;

// operations to get data from database
// foreach($bdd->query("SELECT * from users") as $row) {
//   echo $row[1].'<br>';
// }

// if($data['action'] == "insert"){
//     $title = $data['title']; 
//     $desc = $data['desc']; 
//     $price = $data['price']; 
//     $pic = $data['pic']; 
//     $q = mysqli_query($con, "INSERT INTO `products` ( `title` , `desc` , `price` , `pic` ) VALUES ('$title', '$desc', '$price', '$pic')"); 
//     if($q){
//     $message['status'] = "success"; 
//     }
//     else{
//     $message['status'] = "error"; 
//     }
//     echo json_encode($message); 
// }