<?php
class API {
    public function __construct() {}

    public function setHeader($origin, $charset, $methods, $max_age) {
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json; charset=UTF-8');
        header('Access-Control-Allow-Methods: *');
        header("Access-Control-Max-Age: 3600");
        header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
    }

}
