<?php
// https://www.codeproject.com/Articles/1005150/Posting-Data-from-Ionic-App-to-PHP-Server
// https://www.youtube.com/watch?v=VroNPi1t3Cc
// Headers to send the request with json
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');
header('Access-Control-Allow-Methods: *');
header("Access-Control-Max-Age: 3600");
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

require "../vendor/autoload.php";
require_once 'connection.php'; 

switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        break;

    case "POST":
        // Get datas sent by Angular from the body request
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        // 867959030002509
        $imei = $request->imei;

        // Get the user from the database
        // $stmt = $bdd->prepare("SELECT * FROM scales WHERE imei=:imei LIMIT 0,1");
        // $stmt = $bdd->prepare("SELECT * FROM scales INNER JOIN scale_reports ON scales.id = scale_reports.scale_id");

        $stmt = $bdd->prepare("SELECT 
            scale_reports.scale_id,
            scales.alias,
            scales.reference, 
            scales.tare,
            scales.notify,
            scale_reports.at, 
            scale_reports.hygrometry, 
            scale_reports.battery_level, 
            scale_reports.temperature, 
            scale_reports.weight 
        FROM scales RIGHT JOIN scale_reports ON scales.id = scale_reports.scale_id 
        WHERE scales.imei=:imei ORDER BY scale_reports.at DESC LIMIT 0,1");

        $stmt->bindParam(':imei', $imei);
        $stmt->execute();

        // Set the resulting array to associative
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        // Get the imei
        $results = $stmt->fetch();

        http_response_code(200);
        echo json_encode($results, true);
        break;

    case "PUT":
        echo("put");
        break;

    case "DELETE":
        echo("delete");
        break;
}
