export class User {
  private id: number;
  private firstName: string;
  private lastName: string;
  private email: string;
  private password: string;

  constructor(
    id: number, 
    lastName: string, 
    firstName: string, 
    password: string, 
    email: string
  ) {
    this.id = id;
    this.lastName = lastName;
    this.firstName = firstName;
    this.password = password;
    this.email = email;
  }
}