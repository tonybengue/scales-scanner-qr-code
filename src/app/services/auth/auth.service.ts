import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { RequestsService } from '../../services/requests/requests.service';
import { FlashService } from '../../services/flash/flash.service';
import { TokenService } from '../../services/token/token.service';

// For the logout button
import { BehaviorSubject } from 'rxjs';

// TODO - Model user
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public authentification: object = {};
  /**
   * Logout button with observable  user login => true for UserService#isLoggedIn using method login() then subscribe to observable isLoggedIn$ in the html and refresh using asyinc | 
   **/
  private isLoggedIn = new BehaviorSubject<boolean>(false);
  public isLoggedIn$ = this.isLoggedIn.asObservable();

  constructor(
    public router: Router,
    public requestsService: RequestsService,
    private flashService: FlashService,
    private tokenService: TokenService
    // private user: User
  ) {
    // Pass the login for the logout button
    if (this.tokenService.isLoggedIn()) {
      this.isLoggedIn.next(true);
    }
  }

  /** 
   * Login throught php
   */
  public login(datas): void {
    // this.isFormSubmitted = true;

    // Send the values and subscribe to response or error
    // this.requestService.http.post(RequestsService.API_URL, datas).subscribe(
    this.requestsService.postForm(RequestsService.LOGIN, datas).subscribe(
      response => {
        this.authentification = response;

        // Token get from the response
        const token = this.authentification["jwt"];

        // Get the email
        const tokenEmail = this.tokenService.getEmailFromToken(token);

        // TODO - Verify the expriation token and store the user in localstorage
        if (datas['email'] == tokenEmail ) {
          this.isLoggedIn.next(true); // pass variable to the template for logout button
          this.tokenService.setToken('ACCESS_TOKEN', {token: token});
          this.flashService.presentToast("Connecté"); // flash message
          this.router.navigateByUrl('/home'); // change of page once connected
        } else {
          this.flashService.errorToast("Problème de connexion");
        }
        
      },
      error => {
        this.flashService.errorToast(error.error.message);
      }
    );
  }

  /**
  * Delete the token and redirect to login page
  */
  public logout(): void {
    this.isLoggedIn.next(false);

    localStorage.removeItem('ACCESS_TOKEN');
    this.router.navigate(['/login']);
  }

  /**
   * Verify if the user if authenticated to allow to access to other pages
   */
  public isAuthenticated(): boolean {
    return this.tokenService.isLoggedIn();
  }
}