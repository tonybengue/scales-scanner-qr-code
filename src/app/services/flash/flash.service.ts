import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})

/*
    animated : If true, the toast will animate.
    buttons : An array of buttons for the toast.
    color : The color to use from your application’s color palette. Default options are: "primary", "secondary", "tertiary", "success", "warning", "danger", "light", "medium", and "dark". For more information on colors, see theming.
    cssClass : Additional classes to apply for custom CSS. If multiple classes are provided they should be separated by spaces.
    duration : How many milliseconds to wait before hiding the toast. By default, it will show until dismiss() is called.
    enterAnimation : Animation to use when the toast is presented.
    header : Header to be shown in the toast.
    keyboardClose : If true, the keyboard will be automatically dismissed when the overlay is presented.
    leaveAnimation : Animation to use when the toast is dismissed.
    message : Message to be shown in the toast.
    mode : The mode determines which platform styles to use.Type"ios" | "md"
    position : The position of the toast on the screen.Type "bottom" | "middle" | "top"
    Default 'bottom'
    translucent : If true, the toast will be translucent. It only applies when the mode is “ios” and the device supports backdrop-filter.
*/
export class FlashService {
  constructor(private toastController: ToastController) {}

  async presentToast(message: any) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      position: 'top',
      color: 'success'
    });
    toast.present();
  }

  async errorToast(message: any) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      position: 'top',
      color: 'danger',
      cssClass: 'toast',
    });
    toast.present();

  }

  // async hideToast() {
  //   this.ionicToastService.HideToast();
  // }
  
}
