import { Injectable } from '@angular/core';

// JWT decoder
import * as Jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  constructor() { }

    /**
     * Decode the token 
    */
    private decodeToken(token: string): any {
      try {
        return Jwt_decode(token);
      } catch (Error) {
        return null;
      }
    }

    /**
     * Get the token decoded
     */
    public getDecodedTokenFromLocalStrorage(): string {
      const token = this.getTokenFromLocalStrorage();
      const tokenDecoded = this.decodeToken(token); // decode the token

      return tokenDecoded;
    }

    /**
     * Time for token verification
     * TODO
     */
    public isTokenExpired(token): boolean {
      var date = new Date();
      if (this.getTokenDate(token) < date.getTime() / 1000){ // / 1000 cause ms
          // console.log('The token has expired');
          return true;
      } else {
          return false;
      }
    }

  /**
   * Verify if the user is logged
   */
    public isLoggedIn() {
      if(localStorage.getItem('ACCESS_TOKEN') !== null) {
        return true
      } 
      return false;
    }

    /**
     * Get the token from the local storage
     */
    public getTokenFromLocalStrorage() {
      return localStorage.getItem('ACCESS_TOKEN');
    }

    /**
     * Get the datas of the token
     */
    public getTokenDatas(token): object {
      const tokenDecoded = this.decodeToken(token)
      const tokenDatas = tokenDecoded['data']; // datas

      return tokenDatas;
    }

    /**
     * Get the email from the decoded token
     */
    public getEmailFromToken(token): string {
      const tokenDatas = this.decodeToken(token);
      const email = tokenDatas["data"]["email"];

      return email;
    }

    /**
     * Get the date of the token
     */
    public getTokenDate(token): number {
      const tokenDecoded = this.decodeToken(token)
      const tokenExpireDate = tokenDecoded["exp"]; // expiration time

      return tokenExpireDate;
    }

    /**
     * Change the JWT Token
     */
    public setToken(string, obj): void  {
      localStorage.setItem(string, JSON.stringify(obj)) !== null;
    }
}
