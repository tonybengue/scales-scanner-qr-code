import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {
  // private api_url: string = 'http://192.168.0.16:8001/pro/boulot/Sbeeh/Ionic%20QRCode/api_php/';
  // public login: string = '${api_url}login.php';
  
  public static LOGIN: string = 'http://192.168.0.16:8001/pro/boulot/Sbeeh/Ionic%20QRCode/api_php/login.php';
  public static IMEI: string = 'http://192.168.0.16:8001/pro/boulot/Sbeeh/Ionic%20QRCode/api_php/imei.php';

  // public static LOGIN: string = 'http://sbeeh.fr/ionic_api/login.php';
  // public static IMEI: string = 'http://sbeeh.fr/ionic_api/imei.php';

  constructor(
    public http: HttpClient,
  ) { }

  public postForm(url, datas) {
    return this.http.post(url, datas);
  }

  public postImei(url, datas) {
    return this.http.post(url, datas);
  }
}
