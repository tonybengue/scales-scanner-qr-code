import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from "@angular/forms";

import { FlashService } from '../../services/flash/flash.service';
import { AuthenticationService } from '../../services/auth/auth.service';

// Validations
import { EmailValidator } from  '../../validators/emailValidator';
import { PasswordValidator } from  '../../validators/passwordValidator';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage {
  public appTitle = "Se connecter";

  // Submission form
  public isFormSubmitted: boolean = false;
  public loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private flashService: FlashService,
    private authentificationService : AuthenticationService,
  ) {
    /* Verify is each input of the login form is ok */
    this.loginForm = this.formBuilder.group({
      email : new FormControl('', [ 
        Validators.required, 
        Validators.email, 
        // EmailValidator.validEmail
      ]),
      password: new FormControl('', [
          Validators.required
      ])
    })
    
    // Initial value for the field.
    // this.loginForm.get('email').setValue("tonybengue@hotmail.fr");
  }

  ngOnInit() {}

  // Error message for the for validation
  public errorMessages = {
    'email': [
      { type : 'required', message: 'L\'email est requis'},
    ],
    'password': [
      { type : 'required', message: 'Le mot de passe est requis'}
    ]
  }

  /** Submit the form */
  public submitForm() {
    if (this.loginForm.invalid) {
      this.flashService.errorToast("Remplissez toutes les valeurs");
      return;
    } else {
      const datas = {
        'email': this.loginForm.value.email,
        'password': this.loginForm.value.password
      };

      this.authentificationService.login(datas);
    }
  }
}
