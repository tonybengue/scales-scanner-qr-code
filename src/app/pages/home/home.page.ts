import { Component } from '@angular/core';

// QR Code
import { BarcodeScannerOptions, BarcodeScanner } from "@ionic-native/barcode-scanner/ngx";
import { TokenService } from '../../services/token/token.service';
import { RequestsService } from '../../services/requests/requests.service';
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  public appTitle = "Scan de QR Code";
  public qrCodeContent;
  public imeiScanned: string;
  public qrCodeKeys;

  // barcodeScannerOptions: BarcodeScannerOptions;

  constructor(
    public tokenService: TokenService,
    public requestsService: RequestsService,
    public barcodeScanner: BarcodeScanner
  ) {
    // TODO - Verify if the token is already ok
    const token = this.tokenService.getTokenFromLocalStrorage();
    this.tokenService.isTokenExpired(token);
  }

  /**
   * Launch the front camera
   */
  public scanCode(): void {
    this.barcodeScanner.scan().then(barcodeData => {
      this.imeiScanned = barcodeData["text"];

      const imeiScannedIntoJson = {
        "imei": this.imeiScanned
      };
      // this.qrCodeContent = barcodeData;
      // this.qrCodeContent = this.requestsService.postForm(RequestsService.IMEI, imei);
      this.qrCodeContent = this.requestsService.postImei(RequestsService.IMEI, imeiScannedIntoJson).subscribe(
        response => {
          this.qrCodeContent = response;
          this.qrCodeKeys = Object.keys(response);
        },
        error => {
          console.log(error);
        }
      );
    });

  }

}

   // this.encodeData = "https://www.FreakyJolly.com";
  //Options
  //   this.barcodeScannerOptions = {
  //     showTorchButton: true,
  //     showFlipCameraButton: true
  //   };
  // }