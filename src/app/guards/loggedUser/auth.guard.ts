import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

// Authentification
import { AuthenticationService } from '../../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedInAuthGuard implements CanActivate {
  constructor (
    private router: Router,
    private authentificationService : AuthenticationService
  ) {}

  /* Authorize the user to connect or not based from php processing */
  canActivate (
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    /* Redirect to home if the user is logged */
    if (this.authentificationService.isAuthenticated()) {
      this.router.navigate(['home']);
      return true;
    } else {
      return true; // allow the redirection to login page
    }
  }
}

