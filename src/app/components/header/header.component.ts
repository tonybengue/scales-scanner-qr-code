import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  constructor(
    public authenticationService: AuthenticationService,
) {
    // console.log(this.authenticationService.isLoggedIn$)
    }
    
  ngOnInit() {}
  
  /**
   * Button logout in the header
   */
  public logout() {
    this.authenticationService.logout();
  }
}
